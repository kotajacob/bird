## Bird

A very simple open source flappy bird clone written with [LÖVE.](https://love2d.org/)

![screenshot](https://paste.cf/f06716386d09bd13a59ba3cf87eaa7bf5f13787f.png)

Libraries used:

1. [Middleclass](https://github.com/kikito/middleclass)
2. [Tick](https://github.com/rxi/tick)

Exports:

- [Linux (.love)](https://paste.cf/bird.love)

- [Windows (.zip)](https://paste.cf/bird.zip)

Copyright (C) 2017  Dakota Walsh
